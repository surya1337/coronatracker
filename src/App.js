import React, {Component} from "react";

import Cards from "./components/Cards/cards";
import Chart from "./components/Chart/chart";
import CountryPicker from "./components/CountryPicker/countryPicker";

import styles from "./App.module.css";

import {fetchData} from "./api";

import covidImage from "./images/img.png";

class App extends Component {
  state = {data: {}, country: ""};
  async componentDidMount() {
    const fetchedData = await fetchData();
    this.setState({data: fetchedData});
  }

  handleCountryChange = async (country) => {
    const fetchedData = await fetchData(country);
    this.setState({data: fetchedData, country: country});
  };

  render() {
    return (
      <div className={styles.container}>
        <img src={covidImage} className={styles.image} alt="COVID-19" />
        <Cards data={this.state.data} />
        <CountryPicker handleCountryChange={this.handleCountryChange} />
        <Chart data={this.state.data} country={this.state.country} />

        <br />
        <br />
        <p>
          Made by{" "}
          <a href="https://www.instagram.com/surya__sv/" target="_blank">
            Surya
          </a>
        </p>
      </div>
    );
  }
}

export default App;
